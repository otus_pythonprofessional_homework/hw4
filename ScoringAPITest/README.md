# Scoring API Tests (ДЗ-3.1)

### Задачи

- [x] 1.1 Разработка модульных тестов на все типы полей (ScoringAPITest/tests/unit/test_fields.py)
- [x] 1.2 Разработка функциональных тестов на систему (ScoringAPITest/tests/func/test_api.py)
- [x] 2 Доработка cases-decorator - вывод теста на котором произошел сбой.
- [x] 3 Реализация в store.py общение к клиент-серверным key-value хранилищем (memcache) в соответствии с интерфейсом в scoring.py

### Тесты

- Запуск unit тестов: `python3 -m unittest discover -s tests/unit'
- Запуск func тестов: `python3 -m unittest discover -s tests/func'
- Запуск всех тестов: `python3 -m unittest discover -s tests/*'
