import json
from pymemcache.client.base import Client
from pymemcache.client.retrying import RetryingClient
from pymemcache.exceptions import MemcacheUnexpectedCloseError

MC_RETRY_DELAY = .2
MC_RETRY_COUNT = 5
MC_EXPIRE = 3600


class JsonSerde(object):
	def serialize(self, key, value):
		if isinstance(value, str):
			return value.encode('utf-8'), 1
		return json.dumps(value).encode('utf-8'), 2
	def deserialize(self, key, value, flags):
		if flags == 1:
			return value.decode('utf-8')
		if flags == 2:
			return json.loads(value.decode('utf-8'))
		raise Exception("Unknown serialization format")

class MemcachedStore:
	def __init__(self, host="localhost", port=11211):
		self.host = host
		self.port = port
		self.client = Client((host, port), serde=JsonSerde())
		self.conn = RetryingClient(self.client, attempts=MC_RETRY_COUNT, retry_delay=MC_RETRY_DELAY)

	def get(self, key):
		return self.conn.get(key)

	def cache_get(self, key):
		return self.conn.get(key)

	def cache_set(self, key, value, expire=MC_EXPIRE):
		return self.conn.set(key, value, expire=expire)
