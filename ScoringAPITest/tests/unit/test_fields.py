import unittest
import api
import datetime
import functools
import logging

logging.basicConfig(level=logging.INFO, format='[%(asctime)s::%(levelname)s] %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
logger = logging.getLogger(__name__)

def cases(cases):
	def decorator(f):
		@functools.wraps(f)
		def wrapper(*args):
			for c in cases:
				new_args = args + (c if isinstance(c, tuple) else (c,))
				try:
					f(*new_args)
				except Exception as err:
					logging.error(f"TEST CASE FAILED!\n\tARGS: {new_args}")#, exc_info=True)
					raise err
		return wrapper
	return decorator


# =============================================================================
# Char ========================================================================
# =============================================================================
class TestCharFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.CharField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': False, 'nullable': False, 'value': None},
		{'required': False, 'nullable': False, 'value': ''},
		{'required': False, 'nullable': False, 'value': ''},
		{'required': False, 'nullable': False, 'value': None},
		{'required': False, 'nullable': True, 'value': 11111},
		{'required': True, 'nullable': True, 'value': []}
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': True, 'nullable': False, 'value': 'test'},
		{'required': True, 'nullable': False, 'value': 'test'},
		{'required': True, 'nullable': True, 'value': 'test'},
		{'required': False, 'nullable': True, 'value': ''},
		{'required': False, 'nullable': True, 'value': None}
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))


# =============================================================================
# Arguments ===================================================================
# =============================================================================
class TestArgumentsFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.ArgumentsField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': True, 'nullable': False, 'value': ''},
		{'required': True, 'nullable': False, 'value': {}},
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': True, 'nullable': True, 'value': {}},
		{'required': False, 'nullable': True, 'value': None},
		{'required': False, 'nullable': True, 'value': {'kv': 1}},
		{'required': False, 'nullable': True, 'value': {'kv1': 'arg1', 'kv2': 'arg2'}},
		{'required': False, 'nullable': True, 'value': {'a': None, 'b': 123, 'c': dict()}},
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))


# =============================================================================
# Email =======================================================================
# =============================================================================
class TestEmailFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.EmailField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': True, 'nullable': False, 'value': ''},
		{'required': True, 'nullable': False, 'value': 'a@a'},
		{'required': True, 'nullable': False, 'value': 'username.com'},
		{'required': True, 'nullable': False, 'value': '.user@name.com'},
		{'required': True, 'nullable': False, 'value': 123},
		{'required': True, 'nullable': False, 'value': 'xss test@mail.com'},
		{'required': True, 'nullable': False, 'value': []},
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': False, 'nullable': True, 'value': ''},
		{'required': False, 'nullable': True, 'value': None},
		{'required': True, 'nullable': False, 'value': 'name.user@mail.ru'},
		{'required': True, 'nullable': False, 'value': 'username@mail.comp.local'},
		{'required': True, 'nullable': False, 'value': 'a@a.com'},
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))


# =============================================================================
# Phone =======================================================================
# =============================================================================
class TestPhoneFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.PhoneField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': True, 'nullable': False, 'value': ''},
		{'required': True, 'nullable': False, 'value': 'phone'},
		{'required': True, 'nullable': False, 'value': '38111234567'},
		{'required': True, 'nullable': False, 'value': '89991234567'},
		{'required': True, 'nullable': False, 'value': 89991234567},
		{'required': True, 'nullable': False, 'value': '+79991234567'},
		{'required': True, 'nullable': False, 'value': []},
		{'required': True, 'nullable': False, 'value': {}},
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': True, 'nullable': True, 'value': 0},
		{'required': True, 'nullable': True, 'value': ''},
		{'required': False, 'nullable': True, 'value': None},
		{'required': True, 'nullable': False, 'value': '79991234567'},
		{'required': True, 'nullable': False, 'value': 79991234567},
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))


# =============================================================================
# Date ========================================================================
# =============================================================================
class TestDateFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.DateField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': False, 'nullable': False, 'value': '1-1-2000'},
        {'required': False, 'nullable': False, 'value': '2000.01.01'},
        {'required': False, 'nullable': False, 'value': 123},
        {'required': False, 'nullable': True, 'value': []},
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': True, 'nullable': False, 'value': '01.01.2000', 'resp': datetime.datetime.strptime('01.01.2000', "%d.%m.%Y")},
		{'required': True, 'nullable': False, 'value': '1.1.2000', 'resp': datetime.datetime.strptime('1.1.2000', "%d.%m.%Y")},
		{'required': True, 'nullable': True, 'value': '', 'resp': None},
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertEqual(self.field.validate(case['value']), case['resp'])


# =============================================================================
# BirthDay ====================================================================
# =============================================================================
class TestBirthDayFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.BirthDayField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': True, 'nullable': False, 'value': ''},
		{'required': False, 'nullable': False, 'value': 'test'},
		{'required': False, 'nullable': False, 'value': '1/2/2003'},
		{'required': False, 'nullable': False, 'value': '01-02-2003'},
		{'required': True, 'nullable': False, 'value': (datetime.datetime.now() - datetime.timedelta(365.25 * 71)).strftime('%d.%m.%Y')},
		{'required': True, 'nullable': False, 'value': (datetime.datetime.now() + datetime.timedelta(10)).strftime('%d.%m.%Y')},
		{'required': False, 'nullable': False, 'value': 122003},
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': False, 'nullable': True, 'value': None},
		{'required': True, 'nullable': False, 'value': '01.02.2003'},
		{'required': True, 'nullable': False, 'value': '1.2.2003'},
		{'required': True, 'nullable': False, 'value': (datetime.datetime.now() - datetime.timedelta(365.25 * 69)).strftime('%d.%m.%Y')},
		{'required': True, 'nullable': True, 'value': ''}
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))


# =============================================================================
# Gender ======================================================================
# =============================================================================
class TestGenderFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.GenderField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': True, 'nullable': False, 'value': '1'},
		{'required': True, 'nullable': False, 'value': 2.0},
		{'required': False, 'nullable': False, 'value': -1},
		{'required': False, 'nullable': False, 'value': []}
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': True, 'nullable': True, 'value': 1},
		{'required': True, 'nullable': False, 'value': 0},
		{'required': False, 'nullable': True, 'value': None},
		{'required': False, 'nullable': False, 'value': 2},
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))


# =============================================================================
# ClientIDs ===================================================================
# =============================================================================
class TestClientIDsFieldSuite(unittest.TestCase):
	def setUp(self):
		self.field = api.ClientIDsField()

	def init_field(self, required, nullable):
		self.field.required = required
		self.field.nullable = nullable

	@cases([
		{'required': True, 'nullable': False, 'value': None},
		{'required': True, 'nullable': True, 'value': '1,2,3'},
		{'required': False, 'nullable': False, 'value': 'error'},
		{'required': True, 'nullable': False, 'value': ['1','2','3']},
		{'required': True, 'nullable': False, 'value': [.1,.2,.3]},
		{'required': True, 'nullable': False, 'value': ('a',12,'3')},
		{'required': True, 'nullable': False, 'value': {}},
		{'required': False, 'nullable': False, 'value': dict()},
	])
	def test_incorrect_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertRaises(ValueError, self.field.validate, case['value'])

	@cases([
		{'required': True, 'nullable': False, 'value': [1, 2, 3]},
		{'required': True, 'nullable': True, 'value': []},
		{'required': False, 'nullable': True, 'value': set()},
		{'required': False, 'nullable': True, 'value': (1, 2)},
		{'required': False, 'nullable': True, 'value': None},
		{'required': False, 'nullable': True, 'value': [1,]},
	])
	def test_correct_value(self, case):
		self.init_field(case['required'], case['nullable'])
		self.assertIsNone(self.field.validate(case['value']))
