import re
import abc
import uuid
import json
import logging
import hashlib
import argparse
from datetime import datetime
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from scoring import get_interests, get_score
from store import MemcachedStore

RE_EMAIL = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"

MIN_AGE_LIMIT = 14
MIN_AGE_LIMIT_DAYS = MIN_AGE_LIMIT * 365.25
MAX_AGE_LIMIT = 70
MAX_AGE_LIMIT_DAYS = MAX_AGE_LIMIT * 365.25
GENDERS = {
	0: 'unknown',
	1: 'male',
	2: 'female'
}


# ======================================================================================================================
# CODE-BLOCK of FIELD-CLASS ============================================================================================
# ======================================================================================================================


class BaseField(abc.ABC):
	def __init__(self, required=False, nullable=True):
		self.required = required
		self.nullable = nullable

	@abc.abstractmethod
	def validate(self, value):
		if value is not None and not isinstance(value, self.types):
			raise ValueError(f"{type(self).__name__} unacceptable type {type(value)} | Not: {self.types}")
		if self.required and value is None:
			raise ValueError(f"{type(self).__name__} is required field")
		if not self.nullable and value in self.nulls:
			raise ValueError(f"{type(self).__name__} must not be empty")


class CharField(BaseField):
	nulls = ('', None)
	types = (str,)

	def validate(self, value):
		super().validate(value)


class ArgumentsField(BaseField):
	nulls = ({}, None)
	types = (dict,)

	def validate(self, value):
		super().validate(value)


class EmailField(CharField):
	def validate(self, value):
		super().validate(value)
		if value and not re.fullmatch(RE_EMAIL, value):
			raise ValueError('Incorrect email format')


class PhoneField(BaseField):
	nulls = (0, '', None)
	types = (int, str)

	def validate(self, value):
		super().validate(value)
		if value in self.nulls:
			return
		if not str(value)[0] == '7' or not len(str(value)) == 11:
			raise ValueError('Phone must start with 7 and has length 11')


class DateField(CharField):
	def validate(self, value):
		super().validate(value)
		if value:
			try:
				return datetime.strptime(value, "%d.%m.%Y")
			except ValueError:
				raise ValueError('Date must match the format: DD.MM.YYYY')
		return


class BirthDayField(DateField):
	def validate(self, value):
		dtobj = super().validate(value)
		if dtobj:
			days_delta = (datetime.now() - dtobj).days
			if days_delta<0:
				raise ValueError(f'Date from future')
			elif days_delta > MAX_AGE_LIMIT_DAYS:
				raise ValueError(f'Max age limit = {MAX_AGE_LIMIT} reached')
			elif days_delta < MIN_AGE_LIMIT_DAYS:
				raise ValueError(f'Min age limit = {MIN_AGE_LIMIT} reached')


class GenderField(BaseField):
	nulls = (None,)
	types = (int,)

	def validate(self, value):
		super().validate(value)
		if value not in self.nulls and value not in GENDERS:
			raise ValueError(f'Gender must be int value from: {list(GENDERS.keys())}')


class ClientIDsField(BaseField):
	nulls = (list(), set(), tuple(), None)
	types = (list, set, tuple)

	def validate(self, value):
		super().validate(value)
		if value:
			for item in value:
				if not isinstance(item, int):
					raise ValueError(f'Elements of <{value.__class__.__name__}> must be <int>')

# ======================================================================================================================
# CODE-BLOCK of REQUESTS-CLASS =========================================================================================
# ======================================================================================================================


class RequestMetaClass(type):
	def __new__(cls, name, bases, dct):
		data_fields = list()
		for field_name, value in dct.items():
			if isinstance(value, BaseField):
				data_fields.append(field_name)
		dct["_extra_fields"] = data_fields
		return super().__new__(cls, name, bases, dct)


class BaseRequest(metaclass=RequestMetaClass):
	def __init__(self, **kwargs):
		for field_name in self._extra_fields:
			setattr(self, field_name, kwargs.get(field_name))
		self.context = dict()

	def validate(self):
		for field_name in self._extra_fields:
			try:
				self.__class__.__dict__[field_name].validate(getattr(self, field_name))
			except ValueError as e:
				raise ValueError(f"ERROR[{field_name}]::{e}")


class ClientsInterestsRequest(BaseRequest):
	client_ids = ClientIDsField(required=True, nullable=False)
	date = DateField(required=False, nullable=True)

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.update_context()

	def update_context(self):
		self.context.update({'nclients': len(self.client_ids) if self.client_ids else 0})


class OnlineScoreRequest(BaseRequest):
	first_name = CharField(required=False, nullable=True)
	last_name = CharField(required=False, nullable=True)
	email = EmailField(required=False, nullable=True)
	phone = PhoneField(required=False, nullable=True)
	birthday = BirthDayField(required=False, nullable=True)
	gender = GenderField(required=False, nullable=True)

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.update_context()

	def _is_not_null(self, *attrs):
		for attr in attrs:
			if getattr(self, attr) in self.__class__.__dict__[attr].nulls:
				return False
		return True

	def update_context(self):
		fields = [fname for fname in self._extra_fields if self._is_not_null(fname)]
		self.context.update({'has': fields})

	def validate(self):
		super().validate()
		if not (self._is_not_null('birthday', 'gender') or
				self._is_not_null('phone', 'email') or
				self._is_not_null('first_name', 'last_name')):
			raise ValueError(f'[Err] Need correct pair phone/email or firstname/lastname or birthday/gender')


class MethodRequest(BaseRequest):
	account = CharField(required=False, nullable=True)
	login = CharField(required=True, nullable=True)
	method = CharField(required=True, nullable=False)
	token = CharField(required=True, nullable=True)
	arguments = ArgumentsField(required=True, nullable=True)

	@property
	def is_admin(self):
		return self.login == ADMIN_LOGIN


def check_auth(req):
	mgcstr = f"{datetime.now().strftime('%Y%m%d%H')}{ADMIN_SALT}" if req.is_admin else f"{req.account}{req.login}{SALT}"
	digest = hashlib.sha512(mgcstr.encode()).hexdigest()
	return digest == req.token


def clients_interests_handler(request, ctx, store):
	try:
		req = ClientsInterestsRequest(**request.arguments)
		req.validate()
		ctx.update(req.context)
	except ValueError as error:
		return f"{error}", HTTPStatus.UNPROCESSABLE_ENTITY

	client_interests = dict()
	for client_id in req.client_ids:
		client_interests[f'client_id{client_id}'] = get_interests(store, client_id)
	return client_interests, HTTPStatus.OK


def online_score_handler(request, ctx, store):
	if request.is_admin:
		return {'score': 42}, HTTPStatus.OK
	try:
		req = OnlineScoreRequest(**request.arguments)
		req.validate()
		ctx.update(req.context)
	except ValueError as error:
		return f"{error}", HTTPStatus.UNPROCESSABLE_ENTITY
	return {'score': get_score(store=store, phone=req.phone, email=req.email, birthday=req.birthday, gender=req.gender,
							   first_name=req.first_name, last_name=req.last_name)}, HTTPStatus.OK


def method_handler(request, ctx, store):
	request_router = {
		'clients_interests': clients_interests_handler,
		'online_score': online_score_handler
	}
	try:
		req = MethodRequest(**request['body'])
		req.validate()
	except ValueError as error:
		return f"{error}", HTTPStatus.UNPROCESSABLE_ENTITY

	if not req.method:
		return "Not implemented method", HTTPStatus.NOT_IMPLEMENTED

	if not check_auth(req):
		return None, HTTPStatus.FORBIDDEN

	resp, code = request_router[req.method](req, ctx, store)
	return resp, code


class MainHTTPHandler(BaseHTTPRequestHandler):
	router = {
		"method": method_handler
	}
	store = MemcachedStore()

	def get_request_id(self, headers):
		return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

	def do_POST(self):
		response, code = {}, HTTPStatus.OK
		context = {"request_id": self.get_request_id(self.headers)}
		request = None
		try:
			data_string = self.rfile.read(int(self.headers['Content-Length']))
			request = json.loads(data_string)
		except Exception as e:
			logging.exception(f"Parse data error: {e}")
			code = HTTPStatus.BAD_REQUEST

		if request:
			path = self.path.strip("/")
			logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
			if path in self.router:
				try:
					response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
				except Exception as e:
					logging.exception(f"Unexpected error: {e}")
					code = HTTPStatus.INTERNAL_SERVER_ERROR
			else:
				code = HTTPStatus.NOT_FOUND

		self.send_response(code)
		self.send_header("Content-Type", "application/json")
		self.end_headers()
		if code.is_success:
			r = {"response": response, "code": code}
		else:
			r = {"error": response or code.description, "code": code}
		context.update(r)
		logging.info(context)
		self.wfile.write(json.dumps(r).encode())
		return


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--port", action="store", type=int, default=8080)
	parser.add_argument("-l", "--log", action="store", default=None)
	args = parser.parse_args()

	logging.basicConfig(filename=args.log, level=logging.INFO, format='[%(asctime)s::%(levelname)s] %(message)s',
						datefmt='%Y.%m.%d %H:%M:%S')
	server = HTTPServer(("localhost", args.port), MainHTTPHandler)
	logging.info("Starting server at %s" % args.port)
	try:
		server.serve_forever()
	except KeyboardInterrupt:
		pass
	server.server_close()
